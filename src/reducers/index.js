import { combineReducers} from 'redux';
import citasReducer from './citasReducer';
import validacionReducer from './validacionReducer';




export default combineReducers({
     //aca defino el state como accedo a en en los componentes
     citas: citasReducer,
     error: validacionReducer
})