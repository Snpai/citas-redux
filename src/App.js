import React from 'react';
//redux
import store from './store';
import {Provider} from 'react-redux';
//--------

import AgregarCita from './componentes/AgregarCita';
import ListadoCitas from './componentes/ListadoCitas';

function App() {
  return (
    <Provider store={store}>
      <div className="container">
        <header>
          <h1 className="text-center">Administrador de Pacientes de veterinaria</h1>
          <div className="row mt-3">
            <div className="col-md-6">
              <AgregarCita/>
            </div>
            <div className="col-md-6">
              <ListadoCitas/>
            </div>
          </div>
        </header>
      </div>
    </Provider>
  );
}

export default App;
